Feature: Manage share package users
  Scenario1: Accessing manage share Package users page
    Given user is in service information page
    When user click on Manage share package users button
    Then user should be navigated to Manage share package users page

  Scenario2: Viewing already listed service in Manage share package user page
    Given user is in Manage share package users page
    When user click on a service
    Then user should be navigated to service details

  Scenario3: Applying Nick name filter to search services with valid information
    Given user is in Manage share package users page
    And user select Nick name from drop down
    And user fill in valid data
    When user click Apply filter
    Then the services should be displayed accordingly

  Scenario4: Applying Nick name filter to search services with invalid information
    Given user is in Manage share package users page
    And user select Nick name from drop down
    And user fill in invalid data
    When user click Apply filter
    Then the services should fail to displayed

  Scenario5: Applying Service No filter to search services with valid information
    Given user is in Manage share package users page
    And user select Service No from drop down
    And user fill in valid data
    When user click Apply filter
    Then the services should be displayed accordingly

  Scenario6: Applying Service No filter to search services with invalid information
    Given user is in Manage share package users page
    And user select Service No from drop down
    And user fill in invalid data
    When user click Apply filter
    Then the services should fail to displayed

  Scenario7: Clearing filter
    Given user apply filter in Manage share package users page
    When user click clear
    Then all filters are removed




