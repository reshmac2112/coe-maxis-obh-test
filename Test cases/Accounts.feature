Feature: Accounts
This feature is to test the Accounts menu and its sub options
Background:
  Given user is logged into OBH SSO and in home page


Scenario1: Accessing Account & Bills page
  Given user click on Accounts menu
  When user clicks on Account & Bills under Accounts
  Then user should be navigated to Account & Bills page

Scenario2: Accessing Usage page
  Given user click on Accounts menu
  When user clicks on Usage under Accounts
  Then user should be navigated to Usage page

Scenario3: Accessing Managed Services page
  Given user click on Accounts menu
  When user clicks on Usage under Accounts
  Then user should be navigated to Usage page

Scenario4: switching to Account & Usage tab in Usage page
  Given user is in Usage page
  When user click on Account & Usage tab
  Then user should be navigated to Account & Bills page

Scenario5: switching to Managed Services tab in Usage page
  Given user is in Usage page
  When user click on Managed Services tab
  Then user should be navigated to Managed Services page

Scenario6: switching to Account & Usage tab in Managed Services page
  Given user is in Managed Services page
  When user click on Account & Usage tab
  Then user should be navigated to Account & Bills page

Scenario5: switching to Usage tab in Managed Services page
  Given user is in Managed Services page
  When user click on Usage tab
  Then user should be navigated to Usage page

Scenario6: searching active accounts with 'View All Accounts' selected in Accounts & Bills page
  Given user is in Accounts & Bills page
  And user select "View All Accounts" from the drop down
  When user search active account
  Then the account details should be displayed

Scenario7: searching inactive accounts with 'View All Accounts' selected in Accounts & Bills page
  Given user is in Accounts & Bills page
  And user select "View All Accounts" from the drop down
  When user search inactive account
  Then the account details should be displayed

Scenario8: searching active accounts with 'View Active Accounts' selected in Accounts & Bills page
  Given user is in Accounts & Bills page
  And user select "View Active Accounts" from the drop down
  When user search active account
  Then the account details should be displayed

Scenario8: searching active accounts with 'View Active Accounts' selected in Accounts & Bills page
  Given user is in Accounts & Bills page
  And user select "View Active Accounts" from the drop down
  When user search active account
  Then the account details should be displayed

Scenario9: searching inactive accounts with 'View Active Accounts' selected in Accounts & Bills page
  Given user is in Accounts & Bills page
  And user select "View Active Accounts" from the drop down
  When user search inactive account
  Then the account details should not be displayed

Scenario 10: Download bills from Account & Bills page
  Given User is in Account & Bills page
  When user click on download icon
  Then a excel file is download with all account details

