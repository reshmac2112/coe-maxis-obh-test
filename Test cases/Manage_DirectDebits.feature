Feature: Manage Direct Debits
Background:
  Given user is in Manage Direct Debits page

Scenario1: User switching from Direct Debit accounts tab to Non Direct Debit accounts tab
  Given user is in Direct Debit accounts tab
  When user click Non Direct Debit accounts tab
  Then user should be navigated to Non Direct Debit accounts tab


Scenario2: User switching from Non Direct Debit accounts tab to Direct Debit accounts tab
  Given user is in Non Direct Debit accounts tab
  When user click Direct Debit accounts tab
  Then user should be navigated to Direct Debit accounts tab

Scenario3: Viewing Terms and Conditions
  Given user is in Direct Debit Accounts tab
  When user click Terms and Conditions
  Then user should be navigated to Terms and Conditions page

#Scenario4: Proceeding with Register for Direct Debit
  #Given user have a Non Direct Debit account
  #And user select the account
  #When user click submit
  #And user click yes to proceed register Direct Debit
  #Then user should be navigated to Register Direct Debit page