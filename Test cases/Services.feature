Feature: Services
This is test the search functionality in Service Management page
 Background:
    Given user is in Account details page
    When user click Services
    Then user should be navigated to Service Management page

  Scenario1: searching active services with 'View All Services' selected in Services Management page
    Given user select "View All Services" from the drop down
    When user search active service
    Then the service details should be displayed

  Scenario2: searching suspended services with 'View All Services' selected in Services Management page
    Given user select "View All Services" from the drop down
    When user search suspended service
    Then the service details should be displayed

  Scenario3: searching share package group with 'View All Services' selected in Services Management page
    Given user select "View All Services" from the drop down
    When user search share package group
    Then the service details should be displayed

  Scenario4: searching suspended services with 'View Suspended Services' selected in Services Management page
    Given user select "View Suspended Services" from the drop down
    When user search suspended service
    Then the service details should be displayed

  Scenario5: searching active services with 'View Suspended Services' selected in Services Management page
    Given user select "View Suspended Services" from the drop down
    When user search active service
    Then the service details fail to display


  Scenario6: searching share package group with 'View Suspended Services' selected in Services Management page
    Given user select "View Suspended Services" from the drop down
    When user search share package group
    Then the service details fail to display

  Scenario7: searching active services with 'View Active Services' selected in Services Management page
    Given user select "View Active Services" from the drop down
    When user search active services
    Then the service details should be displayed

  Scenario8: searching suspended services with 'View Active Services' selected in Services Management page
    Given user select "View Active Services" from the drop down
    When user search suspended service
    Then the service details fail to display

  Scenario9: searching share package group with 'View Active Services' selected in Services Management page
    Given user select "View Active Services" from the drop down
    When user search share package group
    Then the service details fail to display

  Scenario10: searching active services with 'View Share package Group' selected in Services Management page
    Given user select "View Share package Group" from the drop down
    When user search active services
    Then the service details fail to display

  Scenario11: searching share package group with 'View Share package Group' selected in Services Management page
    Given user select "View Share package Group" from the drop down
    When user search share package group
    Then the service details should be displayed

  Scenario12: searching suspended services with 'View Share package Group' selected in Services Management page
    Given user select "View Share package Group" from the drop down
    When user search suspended services
    Then the service details fail to display








