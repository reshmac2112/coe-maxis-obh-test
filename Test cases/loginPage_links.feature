Feature: Login page links

  
Scenario1: Accessing FAQ page
  Given User is in OBH login page
  When User click "View our FAQs"
  Then User should be navigated to FAQ page

Scenario2: Accessing Terms of service page
  Given User is in OBH login page
  When User click "Terms of Service"
  Then User should be navigated to OBH Terms and conditions page

