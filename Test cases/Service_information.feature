Feature: Service Information
  Scenario1: Editing service information with valid email
    Given user is in service information page
    And user click on edit other information
    When user fill all the valid details
    And click save
    Then the other information is updated successfully

  Scenario2: Editing service information with invalid email
    Given user is in service information page
    And user click on edit other information
    When user fill all the valid details but invalid email
    And click save
    Then the other information fail to update
