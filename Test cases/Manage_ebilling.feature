Feature: Manage E-billing
Background:
  Given user is in Manage E-billing page

Scenario1: User opt for e-bills and input valid email
  Given user choose "enable e-bill"
  When user fill in valid email
  And click save
  Then user successfully opted for e-bill

Scenario2: User opt for e-bills and input invalid email
  Given user choose "enable e-bill"
  When user fill in invalid email
  And click save
  Then user fail to opt for e-bill

Scenario3: User opt for enable hard copy and input valid mobile number
  Given user choose "enable sending hard copy"
  When user check on "send SMS alerts"
  And fill valid mobile number
  Then user successfully opted for Enable hard copy

Scenario4: User opt for enable hard copy and input invalid mobile number
  Given user choose "enable sending hard copy"
  When user check on "send SMS alerts"
  And fill invalid mobile number
  Then user fail to opt for Enable hard copy

Scenario5: User choose enable e-bill and then enable hard copy
  Given user choose enable e-bill
  When user choose enable hard copy
  Then enable hard copy only is selected
  And Enable e-bill is unselected

Scenario6: User choose enable hard copy and then enable e-bill
  Given user choose enable hard copy
  When user choose enable e-bill
  Then enable e-bill only is selected
  And Enable hard copy is unselected