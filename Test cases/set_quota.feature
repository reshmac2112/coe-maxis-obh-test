Feature: Set Quota limit

  Background:
    Given user is Manage share package users page

  Scenario1: Setting Quota for single service with valid quota limit
    Given user click on the check box in service record
    When user click set quota
    And fill valid quota limit
    And click save
    Then quota limit is set for the service

  Scenario2: Setting Quota for single service with invalid quota limit
    Given user click on the check box in service record
    When user click set quota
    And fill invalid quota limit
    And click save
    Then quota limit fail to set for the service

  Scenario3: Setting Quota for multiple service with valid quota limit
    Given user click on the check box in heading row
    When user click set quota
    And fill valid quota limit
    And click save
    Then quota limit is set for all the service

  Scenario4: Setting Quota for multiple service with invalid quota limit
    Given user click on the check box in heading row
    When user click set quota
    And fill invalid quota limit
    And click save
    Then quota limit is set for all the service

  Scenario5: Setting Quota limit and back to Manage share package users page
    Given user select the service to set quota
    And user set quota
    When user click back to Manage share package user
    Then user should be navigated to Manage share package user page

  Scenario6: Setting Quota limit and back to Dashboard
    Given user select the service to set quota
    And user set quota
    When user click back to dashboard
    Then user should be navigated to dashboard