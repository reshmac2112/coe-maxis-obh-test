Feature: Sign up
  This is to test the sign up functionality of SSO in different possible scenarios
  
  Background:
  Given user is in OBH sign up page 
  
Scenario1: Sign up with valid data
  Given User fills in all required fields
  When User click "Submit"
  Then user is successfully registered

Scenario2:Sign up with leaving a mandatory field empty
  Given User fills in the fields
  And leaves a mandatory field blank
  When User click"Submit"
  Then User fail to sign up

Scenario3:Sign up with invalid mobile number
  Given User fills in all the fields
  And User enter a invalid mobile number
  When User click "Submit"
  Then User fail to sign up

Scenario4:Sign up with invalid email
  Given User fills in all the fields
  And User enter a invalid email
  When User click "Submit"
  Then User fail to sign up

Scenario5:Sign up without accepting the Terms & Conditions
  Given User fills in all the fields
  When User did not accept the terms & conditions
  Then the sign up button is disabled