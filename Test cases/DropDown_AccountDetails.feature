Feature: Drop down in Account Details page

Background:
  Given user is in Account details page

Scenario1: Navigating to New Line page from Account details page
  When user select "Add a New Line" from drop down
  Then user should be navigated to New Line page
  And the account number should be already filled

Scenario2: Navigating to Fixed Service Requests page from Account details page
  When user select "Add a New Fixed Service" from drop down
  Then user should be navigated to Fixed Service Requests page
  And the account number should be already filled

Scenario3: Navigating to Switch to Maxis page from Account details page
  When user select "Switch to maxis" from drop down
  Then user should be navigated to Switch to Maxis page
  And the account number should be already filled

Scenario4: Navigating to Modify Account VAS page from Account details page
  When user select "Modify VAS" from drop down
  Then user should be navigated to Modify Account VAS page
  And the account number should be already filled

Scenario5: Navigating to Service Transfer Order page from Account details page
  When user select "Transfer a Service" from drop down
  Then user should be navigated to Service Transfer Order page
  And the account number should be already filled

Scenario6: Navigating to Redirect Bills page from Account details page
  When user select "Redirect Bills" from drop down
  Then user should be navigated to Redirect Bills page
  And the account number should be already filled

  Scenario7: Navigating to General Requests page from Account details page
    When user select "Make a New Request" from drop down
    Then user should be navigated to General Requests page
    And the account number should be already filled