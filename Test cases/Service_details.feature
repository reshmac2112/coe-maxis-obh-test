Feature: Service Details
This is to test switching between tabs within Service details page
Background:
  Given user is in obh landing page

  Scenario1: Viewing service details of already listed service in Service Management page
    Given user is in Service Management page
    When user click on a service listed in Service Management page
    Then user should be navigated to Service details page

  Scenario2: Searching and viewing service details
    Given user is in Service Management page
    And user search for a valid service
    When user click on the listed service
    Then user should be navigated to Service details page

  Scenario Outline3:Switching from Service Information tab to other tabs
    Given user is in Service details page
    And in Service Information tab
    When user click on <tabs>
    Examples:
      |tabs|
      |Commitment |
      |Manage Data Quota   |
      |Value Added Services|
    Then user should be navigated to other tab

  Scenario Outline4:Switching from Commitment tab to other tabs
    Given user is in Service details page
    And in Commitment tab
    When user click on <tabs>
    Examples:
      |tabs|
      |Service Information |
      |Manage Data Quota   |
      |Value Added Services|
    Then user should be navigated to other tab

  Scenario Outline5:Switching from Manage Data Quota tab to other tabs
    Given user is in Service details page
    And in Manage Data Quota tab
    When user click on <tabs>
    Examples:
      |tabs|
      |Commitment |
      |Service Information   |
      |Value Added Services|
    Then user should be navigated to other tab

  Scenario Outline6:Switching from Value Added Services tab to other tabs
    Given user is in Service details page
    And in Value Added Services tab
    When user click on <tabs>
    Examples:
      |tabs|
      |Commitment |
      |manage Data Quota   |
      |Service Information|
    Then user should be navigated to other tab