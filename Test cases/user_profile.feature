Feature: User profile

Background:
  Given User is logged into SSO and in home page
  And User click on user name in home page
  When User click on profile
  Then User is in the profile page
Scenario1: Updating user email with valid email ID
  Given User clicks on edit email ID
  When User fill in valid email ID
  And click next
  Then User email ID is successfully updated

Scenario2: Updating user email with invalid email ID
  Given User clicks on edit email ID
  When User fill in invalid email ID
  And click next
  Then User should fail to update email ID

Scenario3: Updating mobile number with valid data
  Given User click on edit mobile number
  When User fills in valid mobile number
  And CLick next
  Then User mobile number updated successfully

Scenario4: Updating mobile number with invalid data
  Given User click on edit mobile number
  When User fills in invalid mobile number
  And CLick next
  Then User should fail to update mobile number

Scenario5: Updating password with valid data
  Given User click on edit password
  When User fill in valid information
  And click next
  Then User password updated successfully

Scenario6: Updating password with incorrect old password and valid new password
  Given User click on edit password
  When User fill in incorrect old password and valid new password
  And click next
  Then User should fail to update password

Scenario7: Updating Password with valid old password and invalid new password
  Given User click on edit password
  When User fill in valid old password and invalid new password
  And click next
  Then User should fail to update password

Scenario8: Updating Secure keyword
  Given User click on edit secure keyword
  When User fill in the new secure keyword
  And click next
  Then User secure keyword is successfully updated