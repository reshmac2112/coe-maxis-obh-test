
Feature: Login
This feature tests the login functionality with different sets of data
Background: 
Given User is in the SSO login page

Scenario1: Login to OBH SSO with valid credentials
Given User fills in valid "username" and "password"
When User click on "login" button
Then User should be successfully logged in


Scenario2: Login to OBH SSO with incorrect username
Given User fills in invalid username
When User click “Next”
Then User should fail to login

Scenario3:Login to OBH SSO with incorrect password
Given User fills in valid username and invalid password
When User click on "login" button
Then User should fail to login
