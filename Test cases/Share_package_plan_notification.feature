Feature: Configuring share package plan notification
  Background:
    Given user is in Manage share package user page
  Scenario: configuring share package plan notification with valid mobile number
    Given user click configure in Manage share package user page
    And user fill in valid mobile number
    When click save
    And proceed with the confirmation
    Then SMS alert is set

  Scenario: configuring share package plan notification with invalid mobile number
    Given user click configure in Manage share package user page
    When user fill in invalid mobile number
    Then Save button is disabled
    And SMS alert should fail to set

  Scenario: configuring share package plan notification with valid email
    Given user click configure in Manage share package user page
    And user fill in valid email
    When click save
    And proceed with the confirmation
    Then Email alert is set

  Scenario: configuring share package plan notification with invalid email
    Given user click configure in Manage share package user page
    When user fill in invalid email
    Then save button is disabled
    And Email alert should fail to set

   Scenario:  configuring share package plan notification with valid email but not accepting confirmation
     Given user click configure in Manage share package user page
     And user fill in valid email
     When click save
     But did not accept confirmation
     Then the configuration is discarded

  Scenario:  configuring share package plan notification with valid mobile number but not accepting confirmation
    Given user click configure in Manage share package user page
    And user fill in valid mobile number
    When click save
    But did not accept confirmation
    Then the configuration is discarded

