Feature: Account Information

Scenario1: Editing Nick name
  Given user is in Account Information page
  And user click on edit Nick name
  And user update the nick name
  When user click save
  Then Nick name is updated

Scenario2: Editing Account Assigned to
  Given user is in Account Information page
  And user click on edit in Account Assigned to
  And user fill all valid details
  When user click save
  Then Data under Account assigned to is updated

Scenario3: Updating Account Assigned to with invalid email and valid mobile number
  Given user is in Account Information page
  And user click on edit in Account Assigned to
  And user fill all valid details but invalid email
  When user click save
  Then Fail to update data under Account assigned to

Scenario4:  Updating Account Assigned to with invalid primary contact number
  Given user is in Account Information page
  And user click on edit in Account Assigned to
  And user fill all valid details but invalid primary contact number
  When user click save
  Then Fail to update data under Account assigned to

#Scenario4:  Updating Account Assigned to with invalid secondary contact number
  #Given user click on edit in Account Assigned to
  #And user fill all valid details but invalid secondary contact number
  #When user click save
  #Then Fail to update data under Account assigned to

Scenario5: Editing Billing Address
  Given user is in Account Information page
  And user click on edit Billing Adress
  And user fill in the address
  When user click save
  Then Billing adress is updated