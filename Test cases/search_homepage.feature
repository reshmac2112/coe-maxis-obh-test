Feature: Search in Home page
This feature is to test search functionality in Home page

Background:
  Given User is in SSO OBH home page
  And User click on search option

Scenario Outline1: user search for account and All selected from drop down
  Given user select "All" from drop down
  When user type <options>
  Examples:
  | options      |
  |Nick name     |
  |Account number|
  |Customer name |
  |Service name  |
  |Service SIM   |

  And press enter
  Then Account and Services specific is displayed if available

Scenario Outline2: user search for account and Nick name selected from drop down
  Given user select "Nick name" from drop down
  When user type <options>
  Examples:
    | options      |
    |Account number|
    |Customer name |
    |Service name  |
    |Service SIM   |

  And press enter
  Then Fail to display Accounts & Services even if available

Scenario Outline3:  user search for account and customer name selected from drop down
  Given user select "Customer name" from drop down
  When user type <options>
  Examples:
    | options      |
    |Account number|
    |Nick name     |
    |Service name  |
    |Service SIM   |

  And press enter
  Then Fail to display Accounts & Services even if available

Scenario Outline4:  user search for account and Account number selected from drop down
  Given user select "Account number" from drop down
  When user type <options>
  Examples:
    | options      |
    |Customer name |
    |Nick name     |
    |Service name  |
    |Service SIM   |

  And press enter
  Then Fail to display Accounts & Services even if available

Scenario Outline5:  user search for account and service name selected from drop down
  Given user select "service name" from drop down
  When user type <options>
  Examples:
    | options      |
    |Account number|
    |Nick name     |
    |Customer name |
    |Service SIM   |

  And press enter
  Then Fail to display Accounts & Services even if available

Scenario Outline6:  user search for account and Service SIM selected from drop down
  Given user select "Service SIM" from drop down
  When user type <options>
  Examples:
    | options      |
    |Account number|
    |Nick name     |
    |Service name  |
    |Customer name |

  And press enter
  Then Fail to display Accounts & Services even if available

Scenario7:  user search for account and Nick name selected from drop down
  Given user select "Nick name" from drop down
  When user type "Nick name"
  And press enter
  Then Accounts & Services is displayed if available

Scenario8:  user search for account and Customer name selected from drop down
  Given user select "Customer name" from drop down
  When user type "Customer name"
  And press enter
  Then Accounts & Services is displayed if available

Scenario9:  user search for account and Account number selected from drop down
  Given user select "Account number" from drop down
  When user type "Account number"
  And press enter
  Then Accounts & Services is displayed if available

Scenario10:  user search for account and service name selected from drop down
  Given user select "service name" from drop down
  When user type "service name"
  And press enter
  Then Accounts & Services is displayed if available

Scenario11:  user search for account and Service SIM selected from drop down
  Given user select "Service SIM" from drop down
  When user type "Service SIM"
  And press enter
  Then Accounts & Services is displayed if available





