Feature: Account details
  This feature is to test the functions in Account details page

  Background:
    Given user is in OBH landing page

Scenario1: Viewing account details of already listed account in Accounts & Bills page
  Given user is in Account & Bills page
  When user click on a account listed in Account & Bills page
  Then user should be navigated to Account details page

Scenario2: Searching and viewing Account details
  Given user is in Account & Bills page
  And user search for a valid account
  When user click on the listed account
  Then user should be navigated to Account details page

Scenario Outline3:Switching from Billing Information tab to other tabs
  Given user is in Account details page
  And in Billing Information tab
  When user click on <tabs>
  Examples:
  |tabs|
  |Account Information |
  |Manage E-billing    |
  |Value Added Services|
  |Manage Direct Debit |
  |View Services       |
  Then user should be navigated to other tab

Scenario Outline4:Switching from Account Information tab to other tabs
  Given user is in Account details page
  And in Account Information tab
  When user click on <tabs>
  Examples:
    |tabs|
    |Billing Information |
    |Manage E-billing    |
    |Value Added Services|
    |Manage Direct Debit |
    |View Services       |
  Then user should be navigated to other tab

Scenario Outline5:Switching from Manage E-billing tab to other tabs
  Given user is in Account details page
  And in Manage E-billing tab
  When user click on <tabs>
  Examples:
    |tabs|
    |Account Information |
    |Billing Information |
    |Value Added Services|
    |Manage Direct Debit |
    |View Services       |
    Then user should be navigated to other tab

Scenario Outline6:Switching from Value Added Services tab to other tabs
  Given user is in Account details page
  And in Value Added Services tab
  When user click on <tabs>
  Examples:
    |tabs|
    |Account Information |
    |Manage E-billing    |
    |Billing information |
    |Manage Direct Debit |
    |View Services       |
  Then user should be navigated to other tab

Scenario Outline7:Switching from Details tab to other tabs
  Given user is in Account details page
  And in Details tab
  When user click on <tabs>
  Examples:
    |tabs|
    |Services |
    |Activate Services|
    |Resume Services  |
  Then user should be navigated to other tab

Scenario Outline8:Switching from Services tab to other tabs
  Given user is in Account details page
  And in Services tab
  When user click on <tabs>
  Examples:
    |tabs|
    |Details |
    |Activate Services|
    |Resume Services  |
  Then user should be navigated to other tab

Scenario Outline9:Switching from Activated Services tab to other tabs
  Given user is in Account details page
  And in Activated Services tab
  When user click on <tabs>
  Examples:
    |tabs|
    |Details |
    |Services|
    |Resume Services  |
  Then user should be navigated to other tab


Scenario Outline10:Switching from Resume Services tab to other tabs
  Given user is in Account details page
  And in Resume Services tab
  When user click on <tabs>
  Examples:
    |tabs|
    |Details |
    |Activate Services|
    |Services  |
  Then user should be navigated to other tab




